/*
 * controller.h
 *
 *  Created on: Nov 4, 2015
 *      Author: giovani and olmer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_CONTROLLER_H_
#define SRC_CONTROLLER_H_

#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif
		double controller(
							double Th, // Theta reference
							double X,  // X point reference
							double Y,  // Y point reference
							double et, // theta error
							double ex, //  X error
							double lamda, // lambda paramenter of the controller
							double pho, // pho: tilt angle of the camera
							double ty, // y axis translation to camera reference
							double tz, // z axis translation to camera reference
							double v // speed...
						  );

#ifdef __cplusplus
}
#endif

#endif /* SRC_CONTROLLER_H_ */
