/*
 * HorizontalLine.hpp
 *
 *  Created on: Nov 6, 2015
 *      Author: Olmer and giovanni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/


#include "HorizontalLine.hpp"

#include <stdio.h>


/***
@brief this function configure the chracteristic of the horizontal line for that receive four parameters
@param y_desired  the point in y coordinate that it will look for
@param width_image the number of pixels in horizontal axis
@param line_resolution  the resolution that you wish in meters to detect the lines. The class convert to number of pixel with the intrinsic parameters
@param width_road_m  the  width of the road(distance between each line). The class convert to number of pixel with the intrinsic parameters
@param n_lines  the  numbers of lines to detect. Default 3 , it not working other number
@TODO: Add the intrinsic parameters
**/

void HorizontalLine::Configure(const int y_desired,const int width_image,const double line_resolution,const double width_road_m, const int n_lines)
{


///this value depend of y and the intrinsic/extrinsic parameters of the camera
///in this case, it is assumed for y_desired=400 1m->130px , and the magnitude change linear.
   double m=1/250.0;
   double m_p=130*(m*y_desired+(1.0-m*400));
   printf("%.3f:%.3f",125.0*y_desired/400.0,m_p);
   pixel_width=round_int(line_resolution*m_p);
   if(pixel_width==0)
	pixel_width=1;
   width_road=round_int(width_road_m*m_p/pixel_width);

  hist_x_ant.resize(int(width_image/pixel_width),0.0);
  hist_x.resize(int(width_image/pixel_width),0.0);
    if (DEBUG){
	printf("%d pixels width of the road and %d pixel resolution for y=%d \n",width_road*pixel_width,pixel_width,y_desired);
     }
  windows_looking=width_road/2;
  this->y_desired=y_desired;
  linePosition.resize(n_lines);
  linePosition[1]=(hist_x.size()/n_lines);
  linePosition[0]=(linePosition[1]-width_road);
  linePosition[2]=(linePosition[1]+width_road);
    if (DEBUG){
	printf("(%d ,%d,%d) \n",linePosition[0],linePosition[1],linePosition[2]);
}
///@TODO: init line position for n_lines


}

/**
@brief default constructor
**/
HorizontalLine::HorizontalLine(){
Configure(400,640,0.02,0.02);
}
/**
@brief constructot with HorizontalLine::configure() paramaters
**/
HorizontalLine::HorizontalLine(int y_desired,const int width_image,const double line_resolution,const double width_road_m){
Configure(y_desired,width_image,line_resolution,width_road_m);
}

/***
@brief this function find the x histogram max value of each line to detect.
It does not update the histogram, just the HorizontalLine::linePosition
@return x the pixel where the maximum is find
**/

int HorizontalLine::lineSensor(const double width_filter,const double max_filter)
{

// find the maximum
//@bug here we should look for the maximum with and array


     double max_value=0;
    int max_index=0;
    for (int j=0;j<hist_x.size();j++)
    {
        if(hist_x[j]>max_value)
	{
          max_index=j;
          max_value=hist_x[j];
        }
    }



//detect the line required
    int prox =round_int((1.0*max_index)/(1.0*hist_x.size())*linePosition.size());
    if(prox==3)
	prox=2;
    if (DEBUG){
        printf("max_k:%d max_value:%f line #:%d max_k_1:%d\n",max_index,max_value,prox,linePosition[prox]);
    }
    int abs_d=0;
    int s_d=0;
    int delta=s_d*abs_d*width_road;//
 //filter the maximum position
///TODO: the bigger question is the width depend of the orientation of vehicle respect the way
    linePosition[prox]=round_int(max_filter*max_index+(1-max_filter)*linePosition[prox]);
    int max_k,min_k;
    for(int i=0; i < linePosition.size();i++ )
    {

     if(i!=prox){
      abs_d=abs(i-prox);
      s_d=sgn<int>(i-prox);
	//width=round_int((1.0-width_filter)*width+(width_filter)*(i-prox)*width_road);
      delta=s_d*abs_d*width_road;
//find the other local maximum
	linePosition[i]=max_index+delta;
	if( max_index+delta<hist_x.size()+windows_looking-1 &&  max_index+delta>-windows_looking+1){
	  max_k=max_index+delta+windows_looking;
	  if(max_k>=hist_x.size())
			max_k=hist_x.size();
	  min_k=max_index+delta-windows_looking;
	  if (min_k<0)
			min_k=0;
	  max_value=0;
	 linePosition[i]=-1;
             for(int k=min_k;k<max_k;k++){
		if(hist_x[k]>max_value)
			{
		          linePosition[i]=k;
		          max_value=hist_x[k];
		        }
		}
	//filter the position aroud the width of the road @TODO: how to know that it is really a maximum??
              //@bug the width estimation is really bad so the width _filter is zero!!!!
	      width_road=(width_filter)*abs(linePosition[i]-max_index)/abs_d+(1-width_filter)*width_road;
              printf("%d\t",width_road*pixel_width);

	}
	linePosition[i]=round_int(max_index+width_road*(i-prox));
      }

    }

   return max_index;
}
