/*
 * HorizontalLine.hpp
 *
 *  Created on: Nov 6, 2015
 *      Author: Olmer and giovanni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef SRC_HORIZONTALLINE_HPP_
#define SRC_HORIZONTALLINE_HPP_

#define DEBUG 3


#include <opencv2/highgui/highgui.hpp>


#include <opencv2/imgproc/imgproc.hpp>
#include <algorithm>
#include <vector>
#include <math.h>


template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
template <typename T>
T clip(const T& n, const T& lower, const T& upper) {
  return std::max(lower, std::min(n, upper));
}

template <typename T>
int round_int( T r ) {
    return (r > 0.0) ? (r + 0.5) : (r - 0.5);
}

/***
this class contain all the required of an horizontal line
**/
class HorizontalLine{

public:
   int pixel_width;  /// resolution in pixels of the histogram
   int width_road;   ///initial width of the road (in histogram units width_road*pixel_width->pixels)
   int y_desired;
   int windows_looking; ///this value describe the width in pixel (it divide by pixel_width) to look for the local max
  std::vector<int> linePosition;
   std::vector<double> hist_x_ant;
   std::vector<double> hist_x;
/***
@brief this function configure the chracteristic of the horizontal line for that receive four parameters
@param y_desired  the point in y coordinate that it will look for
@param width_image the number of pixels in horizontal axis
@param line_resolution  the resolution that you wish in meters to detect the lines. The class convert to number of pixel with the intrinsic parameters
@param width_road_m  the  width of the road(distance between each line). The class convert to number of pixel with the intrinsic parameters
@param n_lines  the  numbers of lines to detect. Default 3 , it not working other number
@TODO: Add the intrinsic parameters
**/

void Configure(const int y_desired,const int width_image,const double line_resolution,const double width_road_m, const int n_lines=3);
/**
@brief default constructor
**/
HorizontalLine();

/**
@brief constructot with HorizontalLine::configure() paramaters
**/
HorizontalLine(int y_desired,const int width_image,const double line_resolution,const double width_road_m);

/***
@brief this function find the x histogram max value of each line to detect.
It does not update the histogram, just the HorizontalLine::linePosition
@return x the pixel where the maximum is find
**/

int lineSensor(const double width_filter,const double max_filter);


};


#endif /* SRC_HORIZONTALLINE_HPP_ */
