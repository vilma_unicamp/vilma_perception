/*
 * vilm_image_node.cpp
 *
 *  Created on: Nov 6, 2015
 *      Author: Olmer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>



#include "RoadLineProcessing.hpp"


//time good image 1443881808.787054 / rosbag play -s 257.1s
//time good image from 395-414s (19s) / rosbag play -s 395s  
//time good image from 450-458s (19s) / rosbag play -s 450s 

//to tun the application  rosrun vilma_perception vilma_image_node _image_transport:=compressed

// rosrun vilma_perception vilma_ime_node /image_raw/compressed _image_transport:=compressed
// Add project to workspace ROS: . ~/vilma_perception_ws/devel/setup.bash

int main(int argc, char **argv)
{


  ros::init(argc, argv, "image_listener");
  ros::NodeHandle nh;

  //ros::Publisher pub = nh.advertise<std_msgs::Int32MultiArray>("ImageControlError", 2);

  image_transport::ImageTransport it(nh);
  RoadLineProcessing myRoad(nh);
  image_transport::Subscriber sub = it.subscribe("/image_raw", 1,boost::bind(&RoadLineProcessing::imageCallback, &myRoad, _1));
  ros::spin();




  return 0;
}
