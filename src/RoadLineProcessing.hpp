/*
 * RoadLineProcessing.hpp
 *
 *  Created on: Oct 26, 2015
 *      Author: giovani and olmer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/


#ifndef SRC_ROADLINEPROCESSING_HPP_
#define SRC_ROADLINEPROCESSING_HPP_

#include <algorithm>

#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>

#include "std_msgs/Float64MultiArray.h"
#include "sensor_msgs/Joy.h"

//#include "../GRANSAC/include/GRANSAC.hpp"
//#include "../GRANSAC/include/LineModel.hpp"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
//#include <pcl/sample_consensus/sac.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/sac_model_line.h>

#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/sample_consensus/sac.h>
#include <pcl/sample_consensus/lmeds.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/rransac.h>
#include <pcl/sample_consensus/msac.h>
#include <pcl/sample_consensus/rmsac.h>
#include <pcl/sample_consensus/mlesac.h>
#include <pcl/sample_consensus/sac_model.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/sample_consensus/sac_model_sphere.h>
#include <pcl/sample_consensus/sac_model_cylinder.h>
#include <pcl/sample_consensus/sac_model_circle.h>
#include <pcl/sample_consensus/sac_model_line.h>
#include <pcl/sample_consensus/sac_model_normal_plane.h>
#include <pcl/sample_consensus/sac_model_parallel_plane.h>


#include "../lsd_1.6/lsd.h"
#include "HorizontalLine.hpp"

#include "controller.h"
#include "PIDLMA.hpp"

typedef pcl::SampleConsensusModelLine<pcl::PointXYZ>::Ptr SampleConsensusModelLinePtr;


/**

class to receive the handle of ros with the image
**/
class RoadLineProcessing{

   int width_image;
   int heigh_image;
   int roiX;
   int roiY;

   std::vector< HorizontalLine > hlines;

   unsigned char *search_mask;
   double *image;

   cv::KalmanFilter *KF;
   cv::Mat_<float> *measurement;

   cv::Mat_<float> *mb_ant;


   Eigen::VectorXf previous_coeffr;

   cv::Mat_<float> *desired_line; // [x,y] (pixels), [m] (radians), [b] (pixels)

   ros::Publisher pubJoystickMA;
   ros::Publisher pubImgResult;
   ros::Subscriber subSensorsMA;
   ros::Subscriber subGamePad;


   std_msgs::Float64MultiArray joystick_ma;
   std_msgs::Float64MultiArray ImgResult;
   double velocity;
int botao;
double presao_freio;

double delta_torque;
int left_steer;
int right_steer;
int left_steer_path;
int right_steer_path;
   double t_ant;
int init_line;

   PIDLMA ex_pid;
   PIDLMA eth_pid;

   PIDLMA vel_pid;
   double vel_ref;
   double vel_ref_max;
   double vel_ref2;

public:
/**
@brief constructor of class with initi with default parameters
@TODO: use rosparam to configure the parameters
**/
RoadLineProcessing(ros::NodeHandle nh);

~RoadLineProcessing();

/**
@brief
ROS callback that return the image from the topic
@param msg sensor_msgs::Image

**/
void imageCallback(const sensor_msgs::ImageConstPtr& msg);

void SensorsMaAcquisition(const std_msgs::Float64MultiArray::ConstPtr &msg);
void GamePadAcquisition(const sensor_msgs::Joy::ConstPtr &msg);

void lineExtraction(const sensor_msgs::ImageConstPtr& msg);

Eigen::VectorXf lineFitting(std::map<int,double>, double *,cv_bridge::CvImagePtr cv_ptr);

double visual_servoing_control(cv::Point2f statePt1, cv::Point2f statePt2, double dt );
void send_data(double driver_wheel, double gas, double brake);

};



#endif /* SRC_ROADLINEPROCESSING_HPP_ */
