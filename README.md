this package contain the VILMA perceptions program. the next programs are implemented:

- **vilma_image_node** This program detect the rigth line of the street and make an image based control based in [Cherubini,2011](http://www.researchgate.net/publication/220104086_Visual_servoing_for_path_reaching_with_nonholonomic_robots) . To detect the line , first use  the algorithm of Line segment detection [RG von Gioi,2012](http://www.ipol.im/pub/art/2012/gjmr-lsd/article.pdf),the initial and final points of the detected lines are classified  to line fitting algorithm using RANSAC. The detected line use a low pass filter int the time to smmoth the detection.

This program has two launch the first one, to run the node with parameters required, and the second one to test the node using a bag file.
```
    #this is the test with bag files
    roslaunch vilma_perception vilma_perception_test.launch ki:=0.0004 kd:=0.001 kp:=0.002 kith:=0.00 kdth:=0.001 kpth:=0.1 kivel:=0.02 kdvel:=0.001 kpvel:=0.05  velref:=40.0 bagfile:=/home/robot/catkin_lma/2015-11-07-20-02-58.bag s:=1

    #to run the node
    roslaunch vilma_perception line_detection.launch ki:=0.0004 kd:=0.001 kp:=0.002 kith:=0.00 kdth:=0.001 kpth:=0.1 kivel:=0.02  kdvel:=0.001 kpvel:=0.05  velref:=10.0

```



